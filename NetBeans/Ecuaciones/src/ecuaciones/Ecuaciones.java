/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecuaciones;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Ecuaciones {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        double a, b, c, x;
        double disc;
        
        System.out.println("Introduce coeficiente A:");
        a = entrada.nextDouble();
        
        System.out.println("Introduce coeficiente B:");
        b = entrada.nextDouble();
        
        System.out.println("Introduce coeficiente C:");
        c = entrada.nextDouble();
        
        if(a == 0 && b == 0){
            System.out.println("Coeficientes no válidos");
        }else{
            disc = (b * b - 4 * a * c);
            
            if(a == 0){
                x = -c / b;
            }else{
                
                if(disc == 0){
                    x = -b / (2 * a);
                    System.out.printf("X = %.2f\n", x);
                }else if(disc < 0){
                    System.out.println("Sin solución real");
                }else{
                    x = (-b + Math.sqrt(disc)) / (2 * a);
                    System.out.printf("X1 = %.2f; ", x);
                    x = (-b - Math.sqrt(disc)) / (2 * a);
                    System.out.printf("X2 = %.2f\n", x);
                }
            }
        }
    }
}
