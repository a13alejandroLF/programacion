/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package valoresnumericos;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class ValoresNumericos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        ArrayList<Double> lista = new ArrayList<>();
        
        double num = 0;
        double numero = 1;
        double numMayor = 0;
        double numMenor = 0;
     
        System.out.println("Introduce números. El cero para salir");
        
        while(numero != 0){
            numero = entrada.nextDouble();
            lista.add(numero);
        }
        
        lista.remove(lista.size() - 1);
        
        for(int i = 0; i < lista.size(); i++){
            num += lista.get(i);
        }
        
        for(int i = 0; i < lista.size(); i++){
            if(lista.get(i) > numMayor){ 
                numMayor = lista.get(i);
            }
        }
        
        numMenor = numMayor;
        
        for(int i = 0; i < lista.size(); i++){
            if(lista.get(i) < numMenor){ 
                numMenor = lista.get(i);
            }
        }
        
        if(lista.isEmpty()){
            System.out.println("No hay datos");
        }else{
            System.out.printf("%s%.2f\n", "Min: ", numMenor);
            System.out.printf("%s%.1f\n", "Max: ", numMayor);
            System.out.printf("%s%.2f\n", "Suma: ", num);
            System.out.printf("%s%.2f\n", "Media: ", num / lista.size());
        }
    }
}
