//: CharAritDemo.java
/** Aritmética con caracteres
* Las variables char pueden manejarse como enteros
*/

package chararitdemo;

/**
 *
 * @author a18juliopc
 */

public class CharAritDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char ch;
        
        ch = 'X'; // Asignamos el caracter X
        System.out.println("ch contiene " + ch);
        
        ch += 1; // Incrementamos en 1 su valor actual
        System.out.println("ch contiene " + ch);
        
        ch = '\u21bb'; // Asignamos un valor numerico de la tabla unicode;
        System.out.println("ch contiene " + ch);
    }
}
