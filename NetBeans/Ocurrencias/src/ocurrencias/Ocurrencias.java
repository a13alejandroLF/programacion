/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ocurrencias;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Ocurrencias {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        String caracter;
        String texto = "";
        String fin = "<>";
        int posicion;
        int contador = 0;
        
        System.out.printf("Introduce un caracter: ");
        caracter = entrada.nextLine();
        
        System.out.println("Escribe lo que quieras (<> para Finalizar)");
        while (texto.indexOf(fin) == -1) {            
            texto += entrada.nextLine();
        }
        
        posicion = texto.indexOf(caracter);
        
        while (posicion != -1) {
            contador++;
            posicion = texto.indexOf(caracter, posicion + 1);
        }
        
        System.out.printf("Apariciones de ′%s′: %d\n", caracter, contador);
    }
    
}
