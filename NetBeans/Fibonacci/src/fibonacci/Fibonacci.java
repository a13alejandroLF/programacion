/*
 * Saca la serie Fibonacci
 */
package fibonacci;

import java.math.BigInteger;

/**
 *
 * @author Pillado
 */
public class Fibonacci {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
         // BigInteger trata los numeros como String
        BigInteger f0 = new BigInteger("0");
        BigInteger f1 = new BigInteger("1");
        BigInteger f2 = new BigInteger("0");
        int cont = 1;
        int n = 10;
        
        for(int i = 1; i <= n; i++){
            f2 = f0;
            // Para sumar
            f0 = f1.add(f0);
            f1 = f2;
            // Compara con -1 0 1
            if(f1.compareTo(new BigInteger("0")) == 1){
                cont++;
                if(cont == 10){
                    System.out.println("\n");
                    cont = 1;
                } else {
                    System.out.print(f1.toString() + "\t");
                }
            }
        }
    }
}
