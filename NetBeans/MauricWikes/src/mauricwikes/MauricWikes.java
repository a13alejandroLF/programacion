/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mauricwikes;


/**
 *
 * @author Pillado
 */
public class MauricWikes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int cuadrado;
        int contador = 0;
        int salto = 1;
        
        while(contador < 100){
            cuadrado = (int) Math.pow(contador, 2);
            
            if(salto == 10){
                System.out.printf("%04d\n", cuadrado);
                salto = 1;
            }else{
                salto++;
                System.out.printf("%04d  ", cuadrado);
            }
            contador++;
        }
    }
}
