//: EscapeDemo.java
/**
* Ejemplos de uso se secuencias de escape
*/

package escapedemo;

/**
 *
 * @author a18juliopc
 */
public class EscapeDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char c = '\t'; // secuencia de tabulacion
        
        System.out.println("\"Este texto\nse va a imprimir\nen varias lineas\"");
        System.err.println("Estas\tpalabras\testan\ttabuladas");
        System.out.println("Y" + c + "estas" + c + "también");
    }
}
