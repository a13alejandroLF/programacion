//: StrDemo.java
package strdemo;

/**
 *
 * @author a18juliopc
 */
public class StrDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String str1 = "En un lugar de La Mancha";
        int len = str1.length();
        System.out.println("str1: \"" + str1 + "\"");
        System.out.println("str1 tiene " + len + " caracteres");
        System.out.println("El primero carácter es: '" + str1.charAt(0) + "'");
        System.out.println("Y el último: '" + str1.charAt(len - 1) + "'");
        
        // Comparando dos cadenas
        String str2 = new String(str1);
        
        System.out.println("str2: \"" + str2 + "\"");
        System.out.println("Comparando str1 y str2...");
        System.out.println("usando == --> " + (str1==str2));
        System.out.println("usando compareTo() --> " + str1.compareTo(str2));
        
        // Asignado una nueva cadena a str2
        String str3 = "de cuyo nombre no quiero acordarme";
        str2 = str1 + ", " + str3;
        
        System.out.println("str2: \"" + str2 + "\"");
        System.out.println("La primera ocurrencia de 'de': " + str2.indexOf("de"));
        System.out.println("La última ocurrencia de 'de': " + str2.lastIndexOf("de"));
    }
}