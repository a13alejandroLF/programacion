/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablamulti;

/**
 *
 * @author a18juliopc
 */
public class TablaMulti {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int num = 0;
        int contador = 0;
        String caracter = "*";
        String fila = caracter.repeat(24);
        
        System.out.println(fila);
        System.out.println("* TABLA DE MULTIPLICAR *");
        System.out.println(fila + "\n");
        
        caracter = "-";
        fila = caracter.repeat(14);
        
        while (contador < 11) {
            
            System.out.println(fila);
            System.out.printf(" TABLA DEL %d\n", contador);
            System.out.println(fila);
            
            while (num < 11) {
                
                System.out.printf(" %d x %d = %d\n", contador, num, contador * num);
                num++;
            }
            
            num = 0;
            contador++;
        }
    }
}
