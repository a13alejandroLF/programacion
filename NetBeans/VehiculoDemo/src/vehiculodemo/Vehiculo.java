/*
 * Plantilla vehiculo
 */
package vehiculodemo;

/**
 *
 * @author a18juliopc
 */
class Vehiculo {

    /**
     * @param args the command line arguments
     */
    
    int numPasajeros;   //Nº de pasajeros
    int capDeposito;    //Capacidad deposito (l)
    double consum100KM; //Consumo (litros a los 100KM)
    
    Vehiculo(int capDeposito, double consum100KM, int numPasajeros){
        this(numPasajeros, consum100KM);
        this.capDeposito = capDeposito;
    }

    Vehiculo(int numPasajeros, double consum100KM) {
        this.numPasajeros = numPasajeros;
        this.consum100KM = consum100KM;
    }
    
    double autonomia(){
        return capDeposito * 100.0 / consum100KM;
    }
    
    double litrosDist(int kms){
        return kms * consum100KM / 100;
    }

    public int getNumPasajeros() {
        return numPasajeros;
    }

    public void setNumPasajeros(int numPasajeros) {
        this.numPasajeros = numPasajeros;
    }
}