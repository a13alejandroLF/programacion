/*
* Crear instancias de la clase Vehiculo
*/
package vehiculodemo;

/**
 *
 * @author a18juliopc
 */
public class VehiculoDemo {
    public static void main(String[] args) {
        
        // Creamos dos nuevas instancias de la clase Vehiculo
        Vehiculo miCoche = new Vehiculo(45, 6.8, 5);
        Vehiculo furgoPepe = new Vehiculo(85, 7.25, 7);
        
        System.out.printf("miCoche puede llevar %d ", miCoche.getNumPasajeros());
        System.out.printf(" personas hasta %.2f kms\n", miCoche.autonomia());
        System.out.printf("furgoPepe puede llevar %d ", furgoPepe.getNumPasajeros());
        System.out.printf(" personas hasta %.2f kms\n", furgoPepe.autonomia());
        
        System.out.printf("miCoche, para recorrer 700Km, necesita %.2f litros\n", miCoche.litrosDist(700));
    }
}