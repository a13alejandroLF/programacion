/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geom;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Circulo2DTest {
    
    public static void imprimeCirulo2D(Circulo2D c){
        System.out.printf("%-10s%12s%n", "ID:", c.getId());
        System.out.printf("%-10s%12s%n", "Posicion:", "(" + c.getCx() + ", " + c.getCy() + ")");
        System.out.printf("%-10s%12.1f%n", "Radio:", c.getRadio());
        System.out.printf("%-10s%12s%n", "Color:", c.getColor());
        System.out.printf("%-10s%12s%n", "Relleno:", c.getRelleno());
    }
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        String ch = "_";
        String rep = ch.repeat(25);
        
        Circulo2D c1 = new Circulo2D(2.5);
        
        System.out.println(rep);
        imprimeCirulo2D(c1);
        
        c1.mover(4, 6);
        
        Circulo2D c2 = new Circulo2D(5, 2, 3);
        System.out.println(rep);
        System.out.println("Lista de objetos:");
        System.out.println(c1);
        System.out.println(c2);
        System.out.printf("La distancia entre ellos es: %.2f\n", c1.getDistancia(c2));
        
        System.out.println(rep);
        System.out.println("Datos del nuevo círculo>");
        System.out.printf("Introduce la cordenada X: ");
        int cx = entrada.nextInt();
        System.out.printf("Introduce la cordenada Y :");
        int cy = entrada.nextInt();
        System.out.printf("Introduce el radio: ");
        double radio = entrada.nextDouble();
        entrada.nextLine();//limpiar buffer del scanner
        System.out.printf("Introduce el color: ");
        String color = entrada.nextLine();
        Circulo2D c3 = new Circulo2D(radio, cx, cy);
        c3.setColor(color);
        c3.setRelleno(true);
        
        System.out.println(rep);
        imprimeCirulo2D(c3);
        
        System.out.println("Lista de objetos:");
        System.out.println(c1);
        System.out.println(c2);
        System.out.println(c3);
    }
}
