/*
 * 
 */
package geom;

/**
 *
 * @author Pillado
 */
public class Circulo2D {
    
    //generar el id
    final static String ID = "Circulo2D#";
    static int numId = 1;
    
    //valores por defecto
    final int DEFOULT_COR = 0;
    final String DEFOULT_COL = "black";
    final boolean DEFOULT_REL = false;
    
    //atributos clase
    String id;
    int cx;
    int cy;
    String color;
    double radio;
    boolean relleno;
    String getId;
    
    //constructor
    Circulo2D(double radio, int cx, int cy){
        this(radio);
        this.cx = cx;
        this.cy = cy;
    }

    Circulo2D(double radio) {
        this.radio = radio;
        this.cx = DEFOULT_COR;
        this.cy = DEFOULT_COR;
        this.color = DEFOULT_COL;
        this.relleno = DEFOULT_REL;
        this.id = ID + numId++;
    }
    
    //metodos
    public void mover(int cx, int cy){
        this.cx = cx;
        this.cy = cy;
    }
    
    //dist = √ (x2 − x1)2 + (y2 − y1)2
    static double getDistancia(Circulo2D ca, Circulo2D cb){
        double dist = Math.sqrt(Math.pow(cb.getCx() - ca.getCx(), 2) + Math.pow(cb.getCy() - ca.getCy(), 2));
        return dist;
    }
    
    double getDistancia(Circulo2D circulo){
        return getDistancia(circulo, this);
    }
    
    @Override
    public String toString() {
        return "<" + getId() + ">:[" + cx + ", " + cy + "; " + radio + "]";
    }
    
    //getters
    public String getId(){
        return id;
    }

    public int getCx() {
        return cx;
    }

    public int getCy() {
        return cy;
    }
    
    public double getRadio(){
        return radio;
    }

    public String getColor() {
        return color;
    }

    public boolean getRelleno() {
        return relleno;
    }
    
    //setters
    public void setColor(String color) {
        this.color = color;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    public void setRelleno(boolean relleno) {
        this.relleno = relleno;
    }
}
