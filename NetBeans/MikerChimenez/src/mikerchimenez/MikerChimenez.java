/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mikerchimenez;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class MikerChimenez {

    /**
     * @param a
     * @return 
     */
    public static String quitarAcentos(String a){
        final String CON_ACENTOS = "ÁÉÍÓÚáéíóú";
        final String SIN_ACENTOS = "AEIOUaeiou";
        
        for(int i = 0; i < a.length() - 1; i++){
            for(int j = 0; j < CON_ACENTOS.length(); j++){
                if(a.charAt(i) == CON_ACENTOS.charAt(j)){
                    a = a.replace(a.charAt(i), SIN_ACENTOS.charAt(j));
                }
            }
        }
        
        return a;
    }
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        String texto = "";
        String frase = "";
        String fin = ".";
        String f1 = "-";
        int casos = 0;
        int contador = 0;
        int ind = 0;
        
        System.out.println("Numero de casos:");
        casos = entrada.nextInt();
        
        if(casos > 0){
            do {
                while (texto.indexOf(fin) == -1) {                
                    texto = entrada.nextLine();
                }
            
                while (frase.indexOf(fin) == -1) {                
                    frase = entrada.nextLine();
                }
            
                if(texto.length() > 0 && frase.length() > 0){
                    texto = texto.toLowerCase();
                    texto = quitarAcentos(texto);
                    frase = frase.toLowerCase();
                    frase = quitarAcentos(frase);
                
                    ArrayList<String> t = new ArrayList<String>();
                    ArrayList<String> f = new ArrayList<String>();
                
                    for(int i = 0; i < texto.length(); i++){
                        t.add(Character.toString(texto.charAt(i)));
                    }
            
                    for(int i = 0; i < t.size(); i++){
                        if(" ".equals(t.get(i))){
                            t.remove(i);
                        }
                    }
                
                    for(int i = 0; i < frase.length(); i++){
                        f.add(Character.toString(frase.charAt(i)));
                    }
            
                    for(int i = 0; i < f.size(); i++){
                        if(" ".equals(f.get(i))){
                            f.remove(i);
                        }
                    }
                
                    for(int i = 0; i < t.size(); i++){
                        if(t.get(i).equals(f.get(ind))){
                            if(ind < f.size() - 1){
                                ind++;
                            }
                        }
                    }
                
                    if(ind + 1 == f.size()){
                        System.out.println("SI");
                    }else{
                        System.out.println("NO");
                    }
                
                    texto = "";
                    frase = "";
                    ind = 0;
                    contador++;
                }
            } while (contador < casos);
        }else{
            
            while (texto.indexOf(f1) == -1) {                
                texto = entrada.nextLine();
            }
            
            while (frase.indexOf(f1) == -1) {                
                frase = entrada.nextLine();
            }
                
            System.out.println();
        }
    }   
}