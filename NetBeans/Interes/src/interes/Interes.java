/*
 * Calcula el interes trascurridos x años
 */
package interes;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Interes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        entrada.useLocale(Locale.US);
        
        //Introduce el capital inicial (€): ​10000
        //Introduce el interés anual (%): ​4,5
        //Introduce el número de años: ​20
        
        System.out.print("Introduce el capital inicial (€): ");
        double capital = entrada.nextDouble();
        
        System.out.print("Introduce el interes anual (%): ");
        double interes = entrada.nextDouble();
        
        System.out.print("Introduce el numero de años: ");
        int anos = entrada.nextInt();
        
        //NOTA:dado un capital inicial ​C​,el capital resultante a un interés del
        //​x ​porcien durante ​n ​años es:
        //C·(1+x/100)n
        String a = "#";
        double resultado = capital * (Math.pow(1 + interes / 100, anos));
        
        System.out.println("#################################");
        System.out.printf("%s%32s\n", a, a);
        System.out.printf("# %-18s%3.2f %c %2c\n", "Capital inicial:", capital, '€', '#');
        System.out.printf("# %-18s%3.2f %c %6c\n", "Interes anual:", interes, '%', '#');
        System.out.printf("# %-18s%2d %c %8c\n", "Periodo:", anos, 'A', '#');
        System.out.printf("%s%32s\n", a, a);
        System.out.printf("# %-18s%3.2f %c %2c\n", "Capital final:", resultado, '€', '#');
        System.out.printf("# %-18s%3.2f %c %2c\n", "Rendimiento:", resultado - capital, '€', '#');
        System.out.printf("%s%32s\n", a, a);
        System.out.println("#################################");
    }
}
