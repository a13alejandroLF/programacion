/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiplicar;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Multiplicar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int num; 
        int contador = 0;
        
        System.out.println("Escribe un numero entre 0 y 9:");
        num = entrada.nextInt();
        
        if(num < 0 || num > 9){
            System.out.println("Número no válido");
        }else{
            while (contador < 11) {
                System.out.printf("%d x %d = %d\n", num, contador, num * contador);
                contador++;
            }
        }
    }
}
