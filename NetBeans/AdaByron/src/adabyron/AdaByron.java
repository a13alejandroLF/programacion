/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adabyron;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class AdaByron {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int numero = 1;
        int potencia = 1;
        long suma = 0;
     
        System.out.println("Introduce dos números. El cero para salir");
        
        while(numero != 0){
            numero = entrada.nextInt();
            potencia = entrada.nextInt();
            
            if(numero != 0 && potencia != 0){
                if(numero > 0 && potencia > 0){
                    if(numero <= 10 && potencia <= 10){
                        for(int i = 1; i <= numero; i++){
                            suma += Math.pow(i, potencia);
                        }
                        System.out.println(suma);
                        suma = 0;
                    }
                }
            }
        }
    }
}