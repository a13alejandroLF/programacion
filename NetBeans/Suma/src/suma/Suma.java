/**
* Suma.java
* Suma dos enteros introducidos por el usuario
*/

package suma;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Suma {

    public static void main(String[] args) {
        // declaramos las variables que almacenarán los datos introducidos y el resultado de la suma
        int num1;
        int num2;
        int suma;
        
        // creamos el bjeto Scanner para obtener la entrada del usuario
        Scanner entrada = new Scanner(System.in);
        
        // leemos el primer número
        System.out.print("Escriba el primer numero: ");
        num1 = entrada.nextInt(); // lectura
        
        // leemos el segundo número
        System.out.print("Escriba el segundo numero: ");
        num2 = entrada.nextInt(); // lectura
        
        // calculamos y almacenamos la suma
        suma = num1 + num2;
        
        // mostramos el resultado
        System.out.println("La suma de " + num1 + " y " + num2 + " es " + suma);
    }   
}