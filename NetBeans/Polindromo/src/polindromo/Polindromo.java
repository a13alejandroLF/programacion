/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polindromo;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Polindromo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        String texto, reves = "";
        
        System.out.println("Introduce un texto:");
        texto = entrada.nextLine();
        
        texto = texto.replaceAll("\\s", "");
        texto = texto.replaceAll("\\p{Punct}","");
        texto = texto.toLowerCase();
        
        for(int i = texto.length() - 1; i >= 0; i--){
            reves += texto.charAt(i);
        }
        
        if (texto.equals(reves)) {
            System.out.println("Es palíndromo");
        }else{
            System.out.println("No es palíndromo");
        }
    }
}
