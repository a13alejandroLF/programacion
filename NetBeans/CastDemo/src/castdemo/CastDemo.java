//: CastDemo.java
/**
* Ejemplos de uso de <i>cast</i>
* @author bowman
* @version 1.0
*/
package castdemo;

/**
 *
 * @author a18juliopc
 */
public class CastDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double x, y;
        byte b;
        int i;
        char ch;
        
        x = 10.0;
        y = 3.0;
        
        i = (int) (x / y);  // truncamiento del resultado
        System.out.println("La división x/y es " + i);
        
        i = 100;
        b = (byte) i;   // No hay pérdida (100<127)
        System.out.println("El valor de b es " + b);
        
        i = 257;
        b = (byte) i;   // Un byte no puede almacenar 257!!!
        System.out.println("El valor de b es " + b);
        
        i = 88; // ASCII de 'X'
        ch = (char) i;  // No hay pérdida (88<65535)
        System.out.println("El valor de ch es " + ch);
    }   
}