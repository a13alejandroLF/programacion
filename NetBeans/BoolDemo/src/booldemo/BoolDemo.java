//: BoolDemo.java
/**
* Ejemplos de uso de boolean
* @author bowman
* @version 1.0
*/

package booldemo;

/**
 *
 * @author a18juliopc
 */
public class BoolDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean b;
        
        b = false;
        System.out.println("b es " + b);
        
        b = true;
        System.out.println("b es " + b);
        
        //un boolean puede controlar un condicional if
        
        if(b){
            System.out.println("Esto se ejecuta");
        }
        
        b = false;
        
        if(b){
            System.out.println("Esto no se ejecuta");
        }
        
        // El resultado de un operador relacional es un valor boolean
        
        System.out.println("10 > 9 es " + (10 > 9));
    }
    
}
