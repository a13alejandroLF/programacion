/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primo;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Primo {

    public static int esPrimo(int num){
        
        int contador = 0;
        
        if(num < 1){
            return -1;
        }else{
            for(int i = 1; i <= num ; i++){
                if (num % i == 0) {
                    contador++;
                }
            }
        }
        
        if(contador <= 2){
            return 1;
        }else{
            return 0;
        }
    }
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int num;
        
        System.out.println("Escribe un numero:");
        num = entrada.nextInt();
        
        System.out.println(esPrimo(num));
    }
}
