/*
 * Calcula la circuferencia y longitud
 */
package radio;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Radio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        entrada.useLocale(Locale.US);
        
        System.out.print("Introduce el radio: ");
        double radio = entrada.nextDouble();
        
        double longitud = 2 * Math.PI * radio;
        double area = Math.PI * (radio * radio);
        
        System.out.println("=========================================");
        System.out.printf("Longitud de la circuferencia:\t%.2f\n", longitud);
        System.out.printf("Area del circulo:\t\t%.2f\n", area);
        System.out.println("=========================================");
    }
    
}
