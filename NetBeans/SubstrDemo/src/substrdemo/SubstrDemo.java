//: SubstrDemo.java
package substrdemo;

/**
 *
 * @author a18juliopc
 */
public class SubstrDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String str1 = "Vive como si fueras a morir mañana";
        String str2 = "Aprende como si fueras a vivir siempre";
        String str3 = str1 + ";\n" + str2;
        
        System.out.println("\"" + str3 + "\"");
        System.out.println("substring(22, 33): " + str3.substring(22, 34));
        System.out.println("substring(67, 80): " + str3.substring(61, 74));
        
        str3 = str3.replace("\n", " ");
        
        String str4 = "        Mahatma Ghandi       ";
        str4 = str4.trim().toUpperCase();  //: Composición de llamadas a métodos
        System.out.println(str4 + ": \"" + str3 + "\"");
    }
}
