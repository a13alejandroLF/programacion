/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parimpar;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class ParImpar {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int numero;
        
        System.out.println("Introduce un numero:");
        numero = entrada.nextInt();
        
        if (numero == 0) {
            System.out.println("0 es un número par");
        }else if (numero % 2 == 0) {
            System.out.printf("<%d> es un número par\n", numero);
        }else{
            System.out.printf("<%d> es un número impar\n​", numero);
        }
    }
    
}
