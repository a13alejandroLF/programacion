/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primos;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Primos {

    /**
     * @param num
     * @return 
     */
    public static int esPrimo(int num){
        
        int contador = 0;
        
        if(num < 1){
            return -1;
        }else{
            for(int i = 1; i <= num ; i++){
                if (num % i == 0) {
                    contador++;
                }
            }
        }
        
        if(contador <= 2){
            return 1;
        }else{
            return 0;
        }
    }
    
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int num;
        int contador = 1;
        int primo;
        int numMax;
        
        System.out.println("Escribe un numero:");
        num = entrada.nextInt();
        
        if(num > 0){
            while(true){
                if(esPrimo(num) == 1){
                    numMax = num;
                    break;
                }
                num--;
            }
        }else{
            numMax = num;
        }
        
        if(num <= 0){
            System.out.printf("Número no válido\n");
        }else{
            while (contador <= num) {
                primo = esPrimo(contador);
                
                if(primo == 1){
                    if(contador == numMax){
                        System.out.printf("%d\n", contador);
                    }else{
                        System.out.printf("%d,", contador);
                    }
                }
                contador++;
            }
        }
    }
}