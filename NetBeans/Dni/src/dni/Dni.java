/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dni;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Dni {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int dni;
        int num;
        String letra = "TRWAGMYFPDXBNJZSQVHLCKE";
        
        System.out.println("Introduce el DNI (sin la letra):");
        dni = entrada.nextInt();
        
        if (Integer.toString(dni).length() < 8) {
            System.out.println("DNI no válido");
        }else{
            num = dni % 23;
            System.out.printf("%s\n", letra.charAt(num));
        }
    }
}
