/*
 * Convertir a segundos
 */
package convertsegundos;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class ConvertSegundos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("CONVERSOR A SEGUNDOS");
        System.out.println("Numero de dias:");
        int dias = entrada.nextInt();
        System.out.println("Numero de horas:");
        int horas = entrada.nextInt();
        System.out.println("Numero de minutos:");
        int minutos = entrada.nextInt();
        System.out.println("Numero de segundos:");
        int segundos = entrada.nextInt();
        
        int resultado = (dias * 24 * 60 * 60) + (horas * 60 * 60) + (minutos * 60) + segundos;

        System.out.println(dias + " dias, " + horas + " horas, " + minutos + " minutos y " + segundos + " segundos son " + resultado + " segundos");        
    }   
}