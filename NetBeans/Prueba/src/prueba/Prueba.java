/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prueba;

/**
 *
 * @author a18juliopc
 */
public class Prueba {

    /**
     * @param c
     * @param n
     * @return 
     */
    public static String repetir(char c, int n){
        String s = Character.toString(c);
        String r = "";
        
        for(int i = 0; i < n; i++){
            r += s;
        }
        
        return r;
    }
    
    public static void main(String[] args) {
        System.out.println(repetir('#', 6));
    }
    
}
