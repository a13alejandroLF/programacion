/*
 * Redondea nota final de cada evaluacion
 */
package notas;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Notas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        entrada.useLocale(Locale.US);
        
        System.out.println("CALCULO NOTA FINAL");
        
        System.out.print("Introduce la nota de la evaluacion 1: ");
        double notaEva1 = entrada.nextDouble();
        
        System.out.print("Introduce la nota de la evaluacion 2: ");
        double notaEva2 = entrada.nextDouble();
        
        System.out.print("Introduce la nota de la evaluacion 3: ");
        double notaEva3 = entrada.nextDouble();
        
        double media = (notaEva1 + notaEva2 + notaEva3) / 3;
        
        System.out.printf("Nota final (decimal): %.2f\n", media);
        System.out.printf("Nota final (entero): %.0f\n", media);
    }
}
