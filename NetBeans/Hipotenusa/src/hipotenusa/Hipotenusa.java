//: Hipotenusa.java
/**
* Calcula la hipotenusa de un triángulo rectángulo
*/

package hipotenusa;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */

public class Hipotenusa {

    public static void main(String[] args) {
        
        double a, b, h;
        Scanner entrada = new Scanner (System.in);
        
        System.out.println("Introduce el primer cateto");
        a = entrada.nextDouble(); // cateto a
        
        System.out.println("Introduce el segundo cateto");
        b = entrada.nextDouble(); // cateto b
        
        //Invocamos el metodo sqrt de la clase Math para obtener la raiz cuadrada de (a2 + b2)
        h = Math.sqrt(a*a + b*b);
        System.out.println(h);
    }
}