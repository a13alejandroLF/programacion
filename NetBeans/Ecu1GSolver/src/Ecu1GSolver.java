import java.util.Scanner;

class Ecu1GSolver {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.print("Introduce el coeficiente A: ");
        double a = entrada.nextFloat();
        
        System.out.print("Introduce el coeficiente B: ");
        double b = entrada.nextFloat();
        
        if(a != 0) {
            System.out.printf("La solución es X = %.2f%n", -b/a);
        }else if(b != 0) {
            System.out.println("La ecuación no tiene solución");
        }else {
            System.out.println("Indeterminado. La ecuación tiene infinitas soluciones");
        }
        
        System.out.println("Fin programa");
    }
}