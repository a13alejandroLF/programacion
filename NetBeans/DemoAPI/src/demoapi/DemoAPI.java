//: DemoAPI.java
/**
* Ejemplo de llamadas a métodos estáticos de clases del API de Java
*/
package demoapi;

import java.time.LocalTime;         // Importamos la clase LocalTime del paquete java.time
import javax.swing.JOptionPane;     // Importamos la clase JOptionPane del paquete javax.swing

/**
 *
 * @author a18juliopc
 */
public class DemoAPI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /*
        Llamada al método estático showMessageDialog() de la clase JOptionPane para
        que muestre la hora actual, obtenida mediante el método estático now() de la
        clase LocalTime
        */
        JOptionPane.showMessageDialog(null, "La hora actual es: " + LocalTime.now());
    }
}
