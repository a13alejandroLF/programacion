/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mayor;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Mayor {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int num1, num2, num3;
        
        System.out.println("Introduce el primer numero");
        num1 = entrada.nextInt();
        
        System.out.println("Introduce el segundo numero");
        num2 = entrada.nextInt();
        
        System.out.println("Introduce el tercer numero");
        num3 = entrada.nextInt();
        
        if (num1 == num2 && num2 == num3) {
            System.out.println("3 iguales");
        }else{
            if (num1 >= num2 && num1 >= num3) {
                System.out.printf("El mayor es %d\n", num1);
            }else if(num2 >= num1 && num2 >= num3){
                System.out.printf("El mayor es %d\n", num2);
            }else if(num3 >= num1 && num3 >= num2){
                System.out.printf("El mayor es %d\n", num3);
            }
        }
    }
    
}
