/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package multiplo;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 * 
 * Leer el primer numero
 * Leer el segundo numero
 * Si el resto de la division entre el numero uno y el numero dos es cero
 *      numero uno es múltiplo de numero dos​
 * Si no
 *      numero uno no es múltiplo de numero dos 
 */
public class Multiplo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        int num1;
        int num2;
        
        System.out.println("Introduce el primer numero:");
        num1 = entrada.nextInt();
        
        System.out.println("Introduce el segundo numero:");
        num2 = entrada.nextInt();
        
        if(num1 % num2 == 0){
            System.out.printf("<%d> es múltiplo de <%d>\n", num1, num2);
        }else{
            System.out.printf("<%d> no es múltiplo de <%d>\n", num1, num2);
        }
        
    }
    
}
