/*
 * Conversor de Celsius a Fahrenheit
 */
package convertfahrenheit;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class ConvertFahrenheit {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("CONVERSOR DE GRADOS CELSIUS A GRADOS FAHRENHEIT");
        
        System.out.println("Introduce una temperatura en grados Celsius:");
        double celsius = entrada.nextDouble();
        
        double fahrenheit = 1.8 * celsius + 32;
        
        System.out.println(celsius + " ºC son " + fahrenheit + " ºF");
    }
    
}
