/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package granja;

import java.util.Scanner;

/**
 *
 * @author a18juliopc
 */
public class Granja {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int comidaDiaria;
        int numAnimales;
        double kilosPorAnimal;
        double cantidad;
        
        System.out.println("Comida diaria:");
        comidaDiaria = entrada.nextInt();
        System.out.println("Numero de animales");
        numAnimales = entrada.nextInt();
        System.out.println("Kilos por animal");
        kilosPorAnimal = entrada.nextDouble();
        
        cantidad = numAnimales * kilosPorAnimal;
        
        if (comidaDiaria >= cantidad) {
            System.out.printf("Excedente = %.1f Kg", comidaDiaria - cantidad);
        }else{
            System.out.printf("Ración = %.2f Kg", comidaDiaria / numAnimales);
        }
    }
}
