/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caracteres;

/**
 *
 * @author a18juliopc
 */
public class Caracteres {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        final int MASK = 0b10_0000; // 32 decimal
        
        String s = "hOlA";
        String res = "";
        
        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            if(c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'){
                c ^= MASK;
            }
            res += c;
        }
        
        System.out.println(res);
    }
}
