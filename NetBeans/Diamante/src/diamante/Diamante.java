/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package diamante;

/**
 *
 * @author Pillado
 */
public class Diamante {

    /**
     * @param n
     * @param c
     */
    public static void diamante(int n, char c){
        int filas = n;
        int contador = 1;
        int impar;
        int cont;
        String letra = Character.toString(c);
        String espacio = " ";
        String blanco;
        String rep = letra.repeat(contador);
        
        if(n % 2 == 0){
            System.out.println();
        }else{
            while(contador <= filas){
                if(contador == 1){
                    cont = (filas - 1) / 2;
                    blanco = espacio.repeat(cont);
                    System.out.printf("%s%s%s\n", blanco, rep, blanco);
                }else{
                    rep = letra.repeat(contador);
                    impar = rep.length();
                    if(impar % 2 != 0){
                        cont = (filas - impar) / 2 ;
                        blanco = espacio.repeat(cont);
                        System.out.printf("%s%s%s\n", blanco, rep, blanco);
                    }
                }
                contador++;
            }
            contador = filas - 1;
            while(contador > 0){
                if(contador == 1){
                    rep = letra.repeat(1);
                    cont = (filas - 1) / 2;
                    blanco = espacio.repeat(cont);
                    System.out.printf("%s%s%s\n", blanco, rep, blanco);
                }else{
                    rep = letra.repeat(contador);
                    impar = rep.length();
                    if(impar % 2 != 0){
                        cont = (filas - impar) / 2 ;
                        blanco = espacio.repeat(cont);
                        System.out.printf("%s%s%s\n", blanco, rep, blanco);
                    }
                }
                contador--;
            }
        }
    }
    
    public static void main(String[] args) {
        diamante(5, '#');
    }
    
}
